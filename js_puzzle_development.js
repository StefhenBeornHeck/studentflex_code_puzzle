
const alphabetString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const storeLength = 6;

const buildingGrid = alphabetString.split("").reduce((acc, currentVal, index) => {
    if(index % storeLength === 0){
        acc.push([currentVal]);
    }else{
        acc[acc.length-1].push(currentVal);
    }
    return acc;
}, []);

const solution = "side job at studentflex";

const scheduleCoos = solution.split("").map((letter) => {
    if(letter === " "){
        return [0,0];
    }

    return buildingGrid.reduce( (acc, store, i) => {
        const found = store.indexOf(letter.toUpperCase());
        if(found !== -1) acc = [i+1, found+1];
        return acc;
    }, undefined);
});

console.log(buildingGrid);
console.log(scheduleCoos);
