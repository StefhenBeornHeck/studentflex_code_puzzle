// You can copy these variables from *WEBSITE* to get you started quickly!

// Multidimensional array that represents the Ciphix building

//    1st room       ->        6th
const ciphixBuildingGrid = [
    [ 'A', 'B', 'C', 'D', 'E', 'F' ],   // 1st floor
    [ 'G', 'H', 'I', 'J', 'K', 'L' ],
    [ 'M', 'N', 'O', 'P', 'Q', 'R' ],   // ...
    [ 'S', 'T', 'U', 'V', 'W', 'X' ],
    [ 'Y', 'Z' ]                        // 5th
];

// Multidimensional array that represents the student's work schedule
// IMPORTANT! Remember that a [0,0] must be converted to a white space

//   mo        tu        wed       thur      fri
const workSchedule = [
    [ 4, 1 ], [ 2, 3 ], [ 1, 4 ], [ 1, 5 ], [ 0, 0 ],   // week 1
    [ 2, 4 ], [ 3, 3 ], [ 1, 2 ], [ 0, 0 ], [ 1, 1 ],   // week 2
    [ 4, 2 ], [ 0, 0 ], [ 4, 1 ], [ 4, 2 ], [ 4, 3 ],   // week 3
    [ 1, 4 ], [ 1, 5 ], [ 3, 2 ], [ 4, 2 ], [ 1, 6 ],   // week 4
    [ 2, 6 ], [ 1, 5 ], [ 4, 6 ], [ 0, 0 ], [ 0, 0 ]    // week 5
];

console.log(workSchedule[0]);

function solvePuzzle(scheduleCoos){
    return scheduleCoos.map( ([store, lokaal]) => {
        if(store === 0) return " ";
        return buildingGrid[store-1][lokaal-1];
    }).join("");
}

const output = solvePuzzle(workSchedule);

console.log(output);
console.log(ciphixBuildingGrid);


